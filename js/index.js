$(function () {
    var datetime = null;
    var createMessageElements = function (arr) {
        for (var i = 0; i < arr.length; i++) {
            var fullName = arr[i].user.firstName + ' ' + arr[i].user.lastName;
            var mess = $('<div class="item">').html(fullName + ' said:' + ' ' + arr[i].message);
            $('.cont').prepend(mess);
        }
    };
    $.ajax({
        url: 'http://146.185.154.90:8000/blog/dimak007blr@gmail.com/profile',
        method: 'GET'
    }).then(function (result) {
        var wrap = $('<div class="wrap">');
        var h1 = $('<h1 id="name">').append(result.firstName, ' ', result.lastName);
        var edit = $('<button id="edit">').append('<i class="fas fa-edit"></i>');
        edit.attr({
            'type': 'button',
            'class': 'btn btn-primary',
            'data-toggle': 'modal',
            'data-target': '#exampleModal',
            'data-whatever': '@mdo'
        });
        var follow = $('<button class="follow">').append('<i class="fas fa-user-plus"></i>', ' Follow user');

        wrap.append(h1, edit, follow);
        $('body').prepend(wrap);
        $('.follow').click(function (e) {
            e.preventDefault();
            $('.form2').show();
        });
        $('.add').click(function (e) {
            e.preventDefault();
            $.post('http://146.185.154.90:8000/blog/dimak007blr@gmail.com/subscribe',
                {email: $('#email').val()});
            $('.form2').hide();
        });

    });

    $.ajax({
        url: 'http://146.185.154.90:8000/blog/dimak007blr@gmail.com/posts',
        method: 'GET'
    }).then(function (result) {
        datetime = result[result.length - 1].datetime;
        createMessageElements(result);
        setInterval(function () {
            $.ajax({
                url: 'http://146.185.154.90:8000/blog/dimak007blr@gmail.com/posts?datetime=' + datetime,
                method: 'GET'
            }).then(function (result) {
                if (result.length > 0) {
                    createMessageElements(result);
                    datetime = result[result.length - 1].datetime;
                }
            })
        }, 2000);
    });


    $('.btn-primary').on('click', function (e) {
        e.preventDefault();
        $.post('http://146.185.154.90:8000/blog/dimak007blr@gmail.com/profile',
            {firstName: $('#recipient-name').val(), lastName: $('#recipient-name2').val()});
        location.reload();
        $('.modal fade').hide()
    });

    $('#btn-addMsg').on('click', function (e) {
        e.preventDefault();
        if ($('#message').val() !== '') {
            $.post('http://146.185.154.90:8000/blog/dimak007blr@gmail.com/posts', {message: $('#message').val()})
        } else {
            alert('Вы пытаетесь ввести пустую строку.Error!')
        }
    });
    $('#bTn').click(function (e) {
        e.preventDefault();
        $.post('http://146.185.154.90:8000/blog/dimak007blr@gmail.com/subscribe/delete');

    });
    $('#getSubs').click(function (e) {
        e.preventDefault();
        $.get('http://146.185.154.90:8000/blog/dimak007blr@gmail.com/subscribe').then(function (value) {
            console.log(value);
        })
    })

});

